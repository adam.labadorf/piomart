=======
Credits
=======

Development Lead
----------------

* Diego Crespo <diegocrespo@protonmail.com>

Contributors
------------

None yet. Why not be the first? See: CONTRIBUTING.rst
